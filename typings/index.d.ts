import { ObjectId } from "bson";

declare module "betrail-orga-shared" {
    interface IUser {
        _id: any;
        formDatas: any[];
        mangopayDatas: IMangopayData[];
    }
    interface IMangopayData {
        lastname: string;
        firstname: string;
        userId: string;
        walletId?: string;
    }

    interface ITrailEvent {
        _id: string | any;
        name: string;
        code: string;
        events: IEvent[];
        getEvent(eventId: any): any;
        mangoUserId: string;
    }

    interface ITrail {
        _id: string | any;
        name: string;
        code: string;
        events: IEvent[];
        tshirt?: boolean;
        getEvent(eventId: any): any;
        mangoUserId: string;
    }
    interface IEvent {
        date: Date;
        closed: boolean;
        signupStartDate: Date;
        signupEndDate: Date;
        id: string;
        routes: IRoute[];
        fee: number;
        mangoWalletId: string;
    }
    interface IRoute {
        id: string;
        name: string;
        registrations: IRegistration[];
        prices: IPrice[];
    }
    interface IPrice {
        validFrom: string;
        validTo: string;
        amount: number;
    }
    interface IRegistration {
        uid: number;
        mail: string;
        firstname: string;
        lastname: string;
        birthdate: string;
        country: string;
        gender: number;
        routeId: string;
        eventId?: string;
        trailId?: string;
        status: string;
        address: string;
        amount: number;
        paymentStatus?: string;
        paymentMethod?: string;
        bib?: number;
        formData?: any;
        paymentInfo?: any;
        transferInfo?: any;
        extraFields?: any;
        type: 'REGISTRATION';
    }
    
    
    interface IRegOrder {
        id : ObjectId;
        type: 'REG_ORDER'
    }
    


    interface ITranslation {
        lang: string;
        translations: any;
    }



    interface IBasket {
        uid: number;
        items: (IRegistration | IRegOrder)[];
    }


    interface IOrder extends IBasket {
        paymentSytems: PaymentSystems;
        paymentMethod: string;
        status: string;
        items: IRegOrder[];
        totalAmount: number;
    }

    type PaymentSystems = 'MANGOPAY' | 'INT_S1'

    interface UndDrupalField {
        und?: [{
            value: string;
        }];
    }
    interface FrDrupalField {
        fr?: [{
            value: string;
        }];
    }
    interface DrupalLink {
        und?: [{
            target_id: string;
        }];
    }
    interface DrupalUser {
        uid: string;
        name: string;
        field_coureur: DrupalLink;
        field_nom: UndDrupalField;
        field_prenom: UndDrupalField;
        field_role: FrDrupalField;
        language: string;
        mail: string;
        coureur?: {
            birthday: number;
        };
        roles: {
            2?: "authenticated user";
            3?: "administrator";
            4?: "runner";
            5?: "event organizer";
        };
        status: string;
    }
}
